import Vehicle from "./Vehicle.js";

class Car extends Vehicle {

  // c'est une propritété de la classe
  handbrakeOn = true

  toggleHandbrakeStatus() {
    this.handbrakeOn = !this.handbrakeOn;
  }

  speedUp(speedToAdd) {
    if (this.handbrakeOn === false) {
      // accélérer
      super.speedUp(speedToAdd);
    }
  }

  // redéfinition de la méthode start
  start() {
    // j'appelle la méthode start du parent Vehicle
    super.start();

    // ajotu des fonctionnalités spécifiques
    console.log("Je met la clé dans le contact");
    console.log("Je tourne la clé");
  }
}

export default Car;
