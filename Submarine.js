import Vehicle from "./Vehicle.js";

class Submarine extends Vehicle {

  currentDepth = 0;

  // redéfinition du constructeur
  constructor(brand, model, maxSpeed, maxDepthValue) {
    // on appelle le constructeur du parent (super constructeur)
    super(brand, model, maxSpeed);

    this.maxDepth = maxDepthValue;
  }

  changeDepth(newDepth) {
    if (newDepth >= 0 && newDepth <= this.maxDepth) {
      this.currentDepth = newDepth;
    }
  }
}

export default Submarine;