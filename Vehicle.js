class Vehicle {

  // propriété
  currentSpeed = 0;

  constructor(brandValue, modelValue, maxSpeedValue) {
    // propriétés
    this.brand = brandValue;
    this.model = modelValue;
    this.maxSpeed = maxSpeedValue;
  }

  speedUp(speedToAdd) {
    this.currentSpeed += speedToAdd;
    if (this.currentSpeed > this.maxSpeed) {
      this.currentSpeed = this.maxSpeed;
    }
  }

  speedDown(speedToSubstract) {
    this.currentSpeed -= speedToSubstract;
  }

  start() {
    console.log("Je rentre dans le véhicule");
  }
}

export default Vehicle;