import Car from "./Car.js";
import Plane from "./Plane.js";
import Submarine from "./Submarine.js";

const clio = new Car('Renault', 'Clio', 140);
const mikro = new Submarine('Mikro', 'Mir 72-026', 80, 130);
const concorde = new Plane('Sud-Aviation', 'Concorde', 2369);

console.log(clio, mikro, concorde);

// ajouter 100km/h à la clio
clio.speedUp(100);
console.log(clio.currentSpeed);
clio.speedUp(10);
console.log(clio.currentSpeed);
clio.speedUp(50);
console.log(clio.currentSpeed);

mikro.speedUp(50);
console.log(mikro.currentSpeed);

console.log("Démarrage de la Clio")
clio.start();
console.log("Démarrage du sous-marin")
mikro.start();